#pragma once
#include <iostream>
#ifndef _TICTACTOE_H__
#define _TICTACTOE_H__
class TicTacToe
{
private:
	char m_board[9] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	int m_numTurns = 1;
	char m_playerTurn = '1';
	char m_winner;
public:
	TicTacToe() {}

	void DisplayBoard() 
	{
		std::cout << m_board[0] << " " << m_board[1] << " " << m_board[2] << "\n";
		std::cout << m_board[3] << " " << m_board[4] << " " << m_board[5] << "\n";
		std::cout << m_board[6] << " " << m_board[7] << " " << m_board[8] << "\n";
	}

	bool IsOver() 
	{
		if (m_board[0] == m_board[1] && m_board[1] == m_board[2])
			return true;

		else if (m_board[3] == m_board[4] && m_board[4] == m_board[5])
			return true;

		else if (m_board[6] == m_board[7] && m_board[7] == m_board[8])
			return true;

		else if (m_board[0] == m_board[3] && m_board[3] == m_board[6])
			return true;

		else if (m_board[1] == m_board[4] && m_board[4] == m_board[7])
			return true;

		else if (m_board[2] == m_board[5] && m_board[5] == m_board[8])
			return true;

		else if (m_board[0] == m_board[4] && m_board[4] == m_board[8])
			return true;

		else if (m_board[2] == m_board[4] && m_board[4] == m_board[6])
			return true;

		else if (m_numTurns == 9)
			return true;

		else 
			return false;
	}

	char GetPlayerTurn() 
	{
		if (m_numTurns % 2 == 0)
			return'2';
		else
			return '1';
	}

	bool IsValidMove(int position)
	{
		if (position == 1 && m_board[0] != '1')
		{
			return false;
		}
		else if (position == 2 && m_board[1] != '2')
		{
			return false;
		}
		else if (position == 3 && m_board[2] != '3')
		{
			return false;
		}
		else if (position == 4 && m_board[3] != '4')
		{
			return false;

		}
		else if (position == 5 && m_board[4] != '5')
		{
			return false;
		}

		else if (position == 6 && m_board[5] != '6')
		{
			return false;
		}
		else if (position == 7 && m_board[6] != '7')
		{
			return false;
		}
		else if (position == 8 && m_board[7] != '8')
		{
			return false;
		}
		else if (position == 9 && m_board[8] != '9')
		{
			return false;
		}
		else 
		{
			return true;
		}
	}

	void Move(int position)
	{
		char mark;
		char find = GetPlayerTurn();
		if (find == '1')
		{
			mark = 'X';
		}
		else 
		{
			mark = 'O';
		}
		if (position == 1 && m_board[0] == '1')
		{
			m_board[0] = mark;
		}
		else if (position == 2 && m_board[1] == '2')
		{
			m_board[1] = mark;
		}
		else if (position == 3 && m_board[2] == '3')
		{
			m_board[2] = mark;
		}
		else if (position == 4 && m_board[3] == '4')
		{
			m_board[3] = mark;
		}
		else if (position == 5 && m_board[4] == '5')
		{
			m_board[4] = mark;
		}

		else if (position == 6 && m_board[5] == '6')
		{
			m_board[5] = mark;
		}
		else if (position == 7 && m_board[6] == '7')
		{
			m_board[6] = mark;
		}
		else if (position == 8 && m_board[7] == '8')
		{
			m_board[7] = mark;
		}
		else if (position == 9 && m_board[8] == '9')
		{
			m_board[8] = mark;
		}
		m_numTurns++;
	}

	void DisplayResult()
	{
		if (m_numTurns % 2 == 0)
		{
			m_winner = '1';
		}
		else 
		{
			m_winner = '2';
		}
		if (m_numTurns == 9)
		{
			std::cout << "The game has ended with a tie. \n";
		}
		else
		{
			std::cout << "The winner is Player " << m_winner << "\n";
		}
	}
};



#endif // !_TICTACTOE_H__
